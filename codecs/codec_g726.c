/*
 * CallWeaver -- An open source telephony toolkit.
 *
 * Copyright (C) 1999 - 2005, Digium, Inc.
 *
 * Mark Spencer <markster@digium.com>
 *
 * See http://www.callweaver.org for more information about
 * the CallWeaver project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */

/*! \file
 *
 * \brief codec_g726_32kbps.c - Translate between signed linear and ITU G.726-32kbps
 *                              Note that the underlying codec is capable of handling
 *                              all 4 bit rates of G.726, but this file only handles
 *                              the 32kbps variant.
 *
 */

#include <fcntl.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "asterisk.h"

#include "asterisk/lock.h"
#include "asterisk/module.h"
#include "asterisk/config.h"

#include "asterisk/translate.h"
#include "asterisk/utils.h"

#include "g726/g726.h" 

#define BUFFER_SAMPLES     8096    /* size for the translation buffers */
#define BUF_SHIFT       5
#define G726_SAMPLES 160
#define G726_FRAME_LEN 80

/* Sample frame data */
#include "slin_g726_ex.h"
#include "g726_slin_ex.h"

static int useplc = 0;

void ast_fr_init_ex(struct ast_frame *fr,
                     int frame_type,
                    int sub_type,
                    const char *src)
{
    fr->frametype = frame_type;
    fr->subclass = sub_type;
    fr->datalen = 0;
    fr->samples = 0;
    fr->mallocd = 0;
    fr->offset = 0;
    fr->src = src ? src : "";
    fr->data = NULL;
    fr->delivery.tv_sec = 0;
    fr->delivery.tv_usec = 0;
    fr->seqno = 0;
    fr->flags = 0;
    fr->ts = 0;
    fr->len = 0;
}

/*
 * Private workspace for translating signed linear signals to G726.
 */
struct g726_encoder_pvt
{
    g726_state_t g726_state;
    uint8_t buf[BUFFER_SAMPLES];  /* Encoded G726, two nibbles to a word */
};

/*
 * Private workspace for translating G726 signals to signed linear.
 */
struct g726_decoder_pvt
{
    struct ast_frame f;
    uint8_t offset[AST_FRIENDLY_OFFSET];    /* Space to build offset */
    int16_t outbuf[BUFFER_SAMPLES];    /* Decoded signed linear values */
    g726_state_t g726_state;
    int tail;
    plc_state_t plc;
};

/*
 *  Create a new instance of g726_decoder_pvt.
 *
 * Results:
 *  Returns a pointer to the new instance.
 *
 * Side effects:
 *  None.
 */
static int g726tolin_new(struct ast_trans_pvt *pvt)
{
    struct g726_decoder_pvt *tmp = pvt->pvt;
  
    memset(tmp, 0, sizeof(*tmp));
    g726_init(&(tmp->g726_state), 32000, G726_ENCODING_LINEAR, G726_PACKING_LEFT);
    plc_init(&tmp->plc);

    return 0;
}

/*
 *  Create a new instance of g726_encoder_pvt.
 *
 * Results:
 *  Returns a pointer to the new instance.
 *
 * Side effects:
 *  None.
 */

static int lintog726_new(struct ast_trans_pvt *pvt)
{
    struct g726_encoder_pvt *tmp = pvt->pvt;
  
    memset(tmp, 0, sizeof(*tmp));
    g726_init(&(tmp->g726_state), 32000, G726_ENCODING_LINEAR, G726_PACKING_LEFT);
    return 0;
}

/*
 *  Fill an input buffer with packed 4-bit G726 values if there is room
 *  left.
 *
 * Results:
 *  Foo
 *
 * Side effects:
 *  tmp->tail is the number of packed values in the buffer.
 */
static int g726tolin_framein(struct ast_trans_pvt *pvt, struct ast_frame *f)
{
    struct g726_decoder_pvt *tmp = pvt->pvt;

    if (f->datalen == 0)
    {
        /* Perform PLC with nominal framesize of 20ms/160 samples */
        if ((tmp->tail + 160) > BUFFER_SAMPLES)
        {
            return -1;
        }
        if (useplc)
        {
            plc_fillin(&tmp->plc, tmp->outbuf + tmp->tail, 160);
            tmp->tail += 160;
        }
    }
    else
    {
        if ((tmp->tail + f->datalen*2) > BUFFER_SAMPLES)
        {
            return -1;
        }
        tmp->tail += g726_decode(&(tmp->g726_state),
                                 tmp->outbuf + tmp->tail,
                                 (const uint8_t *) f->data,
                                 f->datalen);
        if (useplc)
            plc_rx(&tmp->plc, tmp->outbuf + tmp->tail - f->datalen*2, f->datalen*2);
    }

    pvt->samples += f->samples;
    pvt->datalen += 2 * f->samples;

    return 0;
}

/*
 *  Convert 4-bit G726 encoded signals to 16-bit signed linear.
 *
 * Results:
 *  Converted signals are placed in tmp->f.data, tmp->f.datalen
 *  and tmp->f.samples are calculated.
 *
 * Side effects:
 *  None.
 */
static struct ast_frame *g726tolin_frameout(struct ast_trans_pvt *pvt)
{
    struct g726_decoder_pvt *tmp = pvt->pvt;

    if (tmp->tail == 0)
        return NULL;
 
    ast_fr_init_ex(&tmp->f, AST_FRAME_VOICE, AST_FORMAT_SLINEAR, __PRETTY_FUNCTION__);
    tmp->f.datalen = tmp->tail*2;
    tmp->f.samples = tmp->tail;
    tmp->f.offset = AST_FRIENDLY_OFFSET;
    tmp->f.data = tmp->outbuf;

    pvt->samples -= tmp->tail;

    tmp->tail = 0;

    return &tmp->f;
}

/*
 *  Fill an input buffer with 16-bit signed linear PCM values.
 *
 * Results:
 *  None.
 *
 * Side effects:
 *  tmp->tail is number of signal values in the input buffer.
 */
static int lintog726_framein(struct ast_trans_pvt *pvt, struct ast_frame *f)
{
    struct g726_encoder_pvt *tmp = pvt->pvt;

	if (pvt->samples + f->samples > BUFFER_SAMPLES) {
		ast_log(LOG_WARNING, "Out of buffer space\n");
		return -1;
	}

    memcpy(tmp->buf + pvt->samples, f->data, f->datalen);
    pvt->samples += f->samples;

    return 0;
}

/*
 *  Convert a buffer of raw 16-bit signed linear PCM to a buffer
 *  of 4-bit G726 packed two to a byte (Big Endian).
 *
 * Results:
 *  Foo
 *
 * Side effects:
 *  Leftover inbuf data gets packed, tail gets updated.
 */
static struct ast_frame *lintog726_frameout(struct ast_trans_pvt *pvt)
{
    struct g726_encoder_pvt *tmp = pvt->pvt;
	int datalen = 0;
	int samples = 0;
  
    if (pvt->samples < 160)
        return NULL;

    while (pvt->samples >= 160) {
        g726_encode(&(tmp->g726_state),
                    pvt->outbuf + datalen,
                    tmp->buf + samples,
                    G726_SAMPLES);
        datalen += G726_FRAME_LEN;
        samples += G726_SAMPLES;
        pvt->samples -= G726_SAMPLES;
    }

    if (pvt->samples)
        memmove(tmp->buf, tmp->buf + samples, pvt->samples *2);

    return ast_trans_frameout(pvt, datalen, samples);
}

static struct ast_frame *g726tolin_sample(void)
{
	static struct ast_frame f = {
		.frametype = AST_FRAME_VOICE,
		.subclass = AST_FORMAT_G726,
		.datalen = sizeof(g726_slin_ex),
		.samples = sizeof(g726_slin_ex) * 2,	/* 2 samples per byte */
        .mallocd = 0,
        .offset = 0,
		.src = __PRETTY_FUNCTION__,
		.data = g726_slin_ex,
	};

	return &f;
}

static struct ast_frame *lintog726_sample(void)
{
	static struct ast_frame f = {
		.frametype = AST_FRAME_VOICE,
		.subclass = AST_FORMAT_SLINEAR,
		.datalen = sizeof(slin_g726_ex),
		.samples = sizeof(slin_g726_ex) / 2,	/* 1 sample per 2 bytes */
        .mallocd = 0,
        .offset = 0,
		.src = __PRETTY_FUNCTION__,
		.data = slin_g726_ex,
	};

	return &f;
}

static struct ast_translator g726tolin = {
	.name = "g726tolin",
	.srcfmt = AST_FORMAT_G726,
	.dstfmt = AST_FORMAT_SLINEAR,
	.newpvt = g726tolin_new,	
	.framein = g726tolin_framein,
    .frameout = g726tolin_frameout,
	.sample = g726tolin_sample,
	.desc_size = sizeof(struct g726_decoder_pvt),
	.buffer_samples = BUFFER_SAMPLES,
	.buf_size = BUFFER_SAMPLES * 2,
	.plc_samples = 160,
};

static struct ast_translator lintog726 = {
	.name = "lintog726",
	.srcfmt = AST_FORMAT_SLINEAR,
	.dstfmt = AST_FORMAT_G726,
	.newpvt = lintog726_new,	
	.framein = lintog726_framein,
    .frameout = lintog726_frameout,
	.sample = lintog726_sample,
	.desc_size = sizeof(struct g726_encoder_pvt),
	.buffer_samples = BUFFER_SAMPLES,
	.buf_size = BUFFER_SAMPLES/2,
};

static void parse_config(void)
{
	struct ast_variable *var;
	struct ast_config *cfg = ast_config_load("codecs.conf");

	if (cfg == NULL)
		return;
	for (var = ast_variable_browse(cfg, "plc"); var; var = var->next) {
		if (!strcasecmp(var->name, "genericplc")) {
			g726tolin.useplc = ast_true(var->value) ? 1 : 0;
			ast_verbose(VERBOSE_PREFIX_3 "codec_g726: %susing generic PLC\n",
					g726tolin.useplc ? "" : "not ");
		}
	}
	ast_config_destroy(cfg);
	return 0;
}

static int reload_module(void)
{
    parse_config();
    return 0;
}

static int unload_module(void)
{
	int res = 0;

	res |= ast_unregister_translator(&g726tolin);
	res |= ast_unregister_translator(&lintog726);

	return res;
}

static int load_module(void)
{
    int res = 0;

    parse_config();

	res |= ast_register_translator(&g726tolin);
	res |= ast_register_translator(&lintog726);

	if (res) {
		unload_module();
		return AST_MODULE_LOAD_FAILURE;
	}	

	return AST_MODULE_LOAD_SUCCESS;
}


AST_MODULE_INFO(ASTERISK_GPL_KEY, AST_MODFLAG_DEFAULT, "ITU G.726-32kbps G726 to/from PCM16 translator",
                .load = load_module,
                .unload = unload_module,
                .reload = reload_module,
                );
